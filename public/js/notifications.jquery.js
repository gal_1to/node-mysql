var host = 'http://nodejs-mrci.rhcloud.com'; //Production Server
//var host = 'http://localhost:3000'; // Development Server


$(document).ready(function() {
    // SOCKET IO DECLARATION
    //var socket = io.connect('http://localhost'); // Socket Production
    var socket = new io(); // Socket Development

    // DATATABLES CUSTOMIZE
    $('#notifications').DataTable({
    	"responsive": true,
    	"ajax": host + '/notifications/all',
    	"order": ([2, 'desc']),
    	"columns": [
        	{render: function(data, type, full, meta){

                var title = '<span class="ntitle" >'+full.title+'</span>'; 
                return title;
            }},
        	{ render: function(data, type, full, meta){
        		return full.body.substring(0, 56).replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
        	}},
        	{ render: function(data, type, full, meta){

                return moment(full.created_at).format('MMM Do YY');
                
            }},
            { "data": "category" },
            { render: function(data, type, full, meta){
            	if(full.active == 1){
            		var html = '<button class="btn btn-success btn-circle" onclick="pubNotification('+full.id+')"><i class="fa fa-check"></i></button>'
            	}else{
            		var html = '<button class="btn btn-danger btn-circle" onclick="pubNotification('+full.id+')"><i class="fa fa-times"></i></button>'
            	}

                //html += '<button class="btn btn-warning btn-circle" onclick="showNotificationForm('+full.id+')"><i class="fa fa-edit"></i></button>'

            	return html;


            }},
            { render: function(data, type, full, meta ){
            	var html = '<button class="btn btn-info btn-circle" onclick="delNotification('+full.id+')"><i class="fa fa-eraser "></i></button>'

            	return html;
            }}
            /*{ "data": "user" }*/
            ]
        });

    // FORM DATA
    $('#publish-article').on('click', function(event){
        // SUBMIT EVENT AND PREVENT DEFAULT
        
        /*var data = {
        	notification_title : $('#notification_title').val(),
        	notification_body : $('#notification_body').val(),
        	notification_cat : $('#notification_cat').val(),
        };*/

        console.log("click en publish");

        var data = {
			notification_title		: $('#grande-articulo h1').text(),
			notification_body		: $('#grande-articulo').html(),
			notification_cat	: $('#article-category').val()
		};

        socket.emit('notificate', data);
        //saveInDataBase(data);
        //event.preventDefault(); 
    });



});

function delNotification(n)
{
$.ajax({
	type    : 'DELETE',
	url     : host + '/notifications/' + n,
	crossDomain: true,
	accept: "application/json",
	contentType: "application/json; charset=UTF-8",
	dataType: 'JSON',
	cache   : false,
	beforeSend  : function(){
            //Before to send request do a animation
            // ...
        },
        error: function(request, status, error){
            // If there is a error do something like show message
            console.log(request.responseText);
        },
        success: function(response){
            // If backend had to send you a response do something
            // ...

            $('.modal-body p').text(response.message);
            if(response.response_code == 200){
            	$('#thumb-msg').addClass('btn-success');
            }else if(response.response_code == 201){
            	$('#thumb-msg').addClass('btn-danger');
            }else{
            	$('#thumb-msg').addClass('btn-warning');
            }
            $('#modal-message').modal('show');
            window.location.replace("/notifications/");
        }
    });
}

function pubNotification(n)
{
    $.ajax({
    	type    : 'POST',
    	url     : host + '/notifications/publish/' + n,
    	crossDomain: true,
    	accept: "application/json",
    	contentType: "application/json; charset=UTF-8",
    	dataType: 'JSON',
    	cache   : false,
	    beforeSend  : function(){
            //Before to send request do a animation
            // ...
        },
        error: function(request, status, error){
            // If there is a error do something like show message
            console.log(request.responseText);
        },
        success: function(response){
            // If backend had to send you a response do something
            // ...

            $('.modal-body p').text(response.message);
            if(response.response_code == 200){
            	$('#thumb-msg').addClass('btn-success');
            }else if(response.response_code == 201){
            	$('#thumb-msg').addClass('btn-danger');
            }else{
            	$('#thumb-msg').addClass('btn-warning');
            }
            $('#modal-message').modal('show');
            window.location.replace("/notifications/");
        }
    });
}

function editNotification(n)
{
    //Notification serialize
    var data = {
        notification_title      : $('#grande-articulo h1').text(),
        notification_body       : $('#grande-articulo').html(),
        notification_cat    : $('#article-category').val()
    };
    $.ajax({
        type    : 'PUT',
        url     : host + '/notifications/' + n,
        crossDomain: true,
        accept: "application/json",
        contentType: "application/json; charset=UTF-8",
        dataType: 'JSON',
        cache   : false,
        beforeSend  : function(){
            //Before to send request do a animation
            // ...
        },
        error: function(request, status, error){
            // If there is a error do something like show message
            console.log(request.responseText);
        },
        success: function(response){
            // If backend had to send you a response do something
            // ...

            $('.modal-body p').text(response.message);
            if(response.response_code == 200){
                $('#thumb-msg').addClass('btn-success');
            }else if(response.response_code == 201){
                $('#thumb-msg').addClass('btn-danger');
            }else{
                $('#thumb-msg').addClass('btn-warning');
            }
            $('#modal-message').modal('show');
            window.location.replace("/notifications/");
        }
    });

}

function saveInDataBase(data)
{
    /*
    *   This method is for send JSON Array to 
    *   Node JS server and the backend will save
    *   Data in DB
    */
    $.ajax({
    	type    : 'POST',
    	url     : 'http://localhost:8888/mayestro/public/articles',
    	crossDomain: true,
    	accept: "application/json",
    	contentType: "application/json; charset=UTF-8",
    	data    : article,
    	dataType: 'JSON',
    	cache   : false,
    	beforeSend  : function(){
            //Before to send request do a animation
            // ...
        },
        error: function(request, status, error){
            // If there is a error do something like show message
            console.log(request.responseText);
        },
        success: function(response){
            // If backend had to send you a response do something
            // ...

            $('.modal-body p').text(response.message);
            if(response.response_code == 200){
            	$('#thumb-msg').addClass('btn-success');
            }else if(response.response_code == 201){
            	$('#thumb-msg').addClass('btn-danger');
            }else{
            	$('#thumb-msg').addClass('btn-warning');
            }
            $('#modal-message').modal('show');
        }
    });

window.location.replace(host + "/notifications/");
}

function showNotificationForm(n)
{
    /*
    *   Method for show form to edit
    *   Notification with Grande JS
    *   And send to backend NodeJS
    */

    console.log(n);

    $.ajax({
        type    : 'GET',
        url     : host + '/notifications/' + n,
        crossDomain: true,
        accept: "application/json",
        contentType: "application/json; charset=UTF-8",
        dataType: 'JSON',
        cache   : false,
        beforeSend  : function(){
            //Before to send request do a animation
            // ...
        },
        error: function(request, status, error){
            // If there is a error do something like show message
            console.log(request.responseText);
        },
        success: function(response){
            // If backend had to send you a response do something
            // ...
            console.log(response);
            var html = response[0].notification_body;
            
            $('#grande-articulo-update').html(html);
        }
    });


    $('#ModalEditForm').modal('show');
}