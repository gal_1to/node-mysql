// ========================================
// server.js
// ========================================

// ========================================
//SETUP SERVER
// ========================================

var express    = require('express');
var app = require('express')();
var http        = require('http').Server(app);
var io = require('socket.io')(http);
var cors = require('cors');
var notifications = express();
var client = express();

var argv = require('optimist').argv;

app.use(express.static('public'));
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
	

// ========================================
// CONFIG CONNECTION OF SOCKET IO
// ========================================


io.on('connection', function(socket){
	console.log('Se ha conectado un usuario...XD');

	//EACH SOCKET CAN RUN DISCONECT EVENT
	socket.on('disconnect', function(){
		console.log('El usuario se ha desconectado...X(')
	});

	// THIS EVENT GET MESSAGE FROM NOTIFICATION'S ADMIN
	// AND EMIT THE MESSAGE TO ALL CLIENTS
	//Evento para recibir el mensaje del formulario en el cliente
	socket.on('notificate', function(msg){
		var current_date = new Date().toISOString();
		msg['notification_title'] = (msg['notification_title'] != '') ? msg['notification_title'] : 'NOTIFICACIÓN SM';

		console.log('titulo' + msg['notification_title']);
		connection.query('INSERT INTO notifications(title, body, id_user, category, created_at) VALUES(?, ?, ?, ?, ?)', [msg['notification_title'], msg['notification_body'], 1, msg['notification_cat'], current_date], function(error, result){

			if(error){
				res.send(error);
			    throw error;
			}else{
				msg['notification_id'] = result['insertId'];
	    		io.emit('notificate', msg);
			}
	 	});
	});
});

// ========================================
// SETUP BODY PARSER
// ========================================
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// ========================================
// SETUP DATABASE CONNECTION
// ========================================

var mysql      = require('mysql');
var DBHost	= (process.env.OPENSHIFT_MYSQL_DB_HOST) ?  process.env.OPENSHIFT_MYSQL_DB_HOST : 'localhost';
var DBUser = (process.env.OPENSHIFT_MYSQL_DB_HOST) ? 'adminIMiAfIp' : 'root';
var DBPassword = (process.env.OPENSHIFT_MYSQL_DB_HOST) ? 'DYJnlic8tDQi' : 'root';
var DBName = (process.env.OPENSHIFT_MYSQL_DB_HOST) ? 'nodejs' : 'node-mysql';

var connection = mysql.createConnection({
  host     : DBHost,
  user     : DBUser,
  password : DBPassword,
  database : DBName
});

connection.connect(function(err){
	if(!err) {
    	console.log("Database is connected ... \n\n");  
	} else {
    	console.log("Error connecting database ... \n\n" + err);  
	}
});

// ========================================
// IMPORT PASSWORD-HASH FOR HASHING STRING LIKE PASSWORDS
// ========================================

var passwordHash = require('password-hash');

notifications.get("/", function(req, res, next){
	
	//WE'RE USING CONNECTION OBJECT FOR RUN A QUERY IN DB

	res.render('notifications/notifications.ejs');
});

notifications.delete("/:id", function(req, res, next){
	
	var notifications_query = "UPDATE notifications SET deleted=1 WHERE id = " + req.params.id ;

	connection.query(notifications_query, function (error, results, fields){
		if (error) {
        	//
        	console.log('Ocurrio un error!!!, corre en circulos' + error);
        	var api_response = {
        		response_code: 300,
        		message: "Ocurrio un error",
        		message_db: error
        	}
        	res.json(api_response);
	    }
	    if (results.affectedRows > 0) {
	    	var api_response = {
        		response_code: 200,
        		message: "Operación éxitosa",
        		message_db: results
        	}
        	res.json(api_response);
	    }else {
	    	var api_response = {
        		response_code: 201,
        		message: "La operación fue éxitosa pero no afecto ningún campo",
        		message_db: results
        	}
        	res.json(api_response);
	    }
	});
});

//Method for update notification
notifications.put("/:id", function(req, res, next){
	
	var notifications_query = "UPDATE notifications SET deleted=1 WHERE id = " + req.params.id ;

	connection.query(notifications_query, function (error, results, fields){
		if (error) {
        	//
        	console.log('Ocurrio un error!!!, corre en circulos' + error);
        	var api_response = {
        		response_code: 300,
        		message: "Ocurrio un error",
        		message_db: error
        	}
        	res.json(api_response);
	    }
	    if (results.affectedRows > 0) {
	    	var api_response = {
        		response_code: 200,
        		message: "Operación éxitosa",
        		message_db: results
        	}
        	res.json(api_response);
	    }else {
	    	var api_response = {
        		response_code: 201,
        		message: "La operación fue éxitosa pero no afecto ningún campo",
        		message_db: results
        	}
        	res.json(api_response);
	    }
	});
});

notifications.post("/publish/:id", function(req, res, next){
	var get_status_notifications = "SELECT active, deleted FROM notifications where id = " + req.params.id;

	connection.query(get_status_notifications, function (error, results, fields){
		if (error) {
        	//
        	console.log('Ocurrio un error!!!, corre en circulos' + error);
        	var api_response = {
        		response_code: 300,
        		message: "Ocurrio un error",
        		message_db: error
        	}
        	res.json(api_response);
	    }else{
	    	if(results[0].active == 1){

				var notifications_query = "UPDATE notifications SET active=0 WHERE id = " + req.params.id;
	    		connection.query(notifications_query, function (error, results, fields){
	    			var api_response = {
		        		response_code: 200,
		        		message: "La publicación esta desactivada.",
		        		message_db: results
		        	}

		        	res.json(api_response);
	    		});
	    	}else{
	    		var notifications_query = "UPDATE notifications SET active=1 WHERE id = " + req.params.id;
	    		connection.query(notifications_query, function (error, results, fields){
	    			var api_response = {
		        		response_code: 200,
		        		message: "La publicación esta publicada.",
		        		message_db: results
		        	}

		        	res.json(api_response);
	    		});
	    	}
	    }
	});
});

notifications.get("/all", function(req, res, next){
	//CODE TO GET ALL NOTIFICATIONS AND SEND TO TEMPLATE

	//WE'RE USING CONNECTION OBJECT FOR RUN A QUERY IN DB
	var notifications_query = "SELECT n.id, n.title, n.body, n.active, n.deleted, n.created_at, c.name as category, u.name as user FROM notifications n INNER JOIN users u ON n.id_user = u.id INNER JOIN notification_category c ON n.category = c.id WHERE n.deleted=0 ORDER BY n.created_at DESC";

	connection.query(notifications_query, function (error, results, fields){
		if (error) {
        	//
        	console.log('Ocurrio un error!!!, corre en circulos' + error);
	    }
	    if (results.length  > 0) {

	        res.json({"data" : results});
	    }
	});
});

notifications.get("/all/:last", function(req, res, next){
	//CODE TO GET ALL NOTIFICATIONS AND SEND TO TEMPLATE


	//WE'RE USING CONNECTION OBJECT FOR RUN A QUERY IN DB
	var notifications_query = "SELECT n.id as notification_id, n.title as notification_title, n.body as notification_body, n.active, n.created_at, c.id as notification_cat, u.name as user FROM notifications n INNER JOIN users u ON n.id_user = u.id INNER JOIN notification_category c ON n.category = c.id WHERE n.active=1 AND n.deleted=0 ORDER BY n.created_at DESC LIMIT " + req.params.last; 

	connection.query(notifications_query, function (error, results, fields){
		if (error) {
        	//
        	console.log('Ocurrio un error!!!, corre en circulos' + error);
	    }
	    if (results.length  > 0) {

	        res.json(results);
	    }
	});
});

notifications.get("/:id", function(req, res, next){
	//CODE TO GET ALL NOTIFICATIONS AND SEND TO TEMPLATE


	//WE'RE USING CONNECTION OBJECT FOR RUN A QUERY IN DB
	var notifications_query = "SELECT n.title as notification_title, n.body as notification_body, n.created_at, c.id as notification_cat, u.name as user FROM notifications n INNER JOIN users u ON n.id_user = u.id INNER JOIN notification_category c ON n.category = c.id WHERE n.active=1 AND n.id=" + req.params.id; 

	connection.query(notifications_query, function (error, results, fields){
		if (error) {
        	//
        	console.log('Ocurrio un error!!!, corre en circulos' + error);
        	var results = {
        		notification_title: 'Error en la consulta',
        		notification_body: error,
        		response_code : 300,
        		created_at: new Date().toISOString(),
        		notification_cat: "Error"
        	};
        	res.json(results);
	    }
	    if (results.length  > 0) {

	        res.json(results);
	    }else{
	    	var results = {
        		notification_title: 'Error en la consulta',
        		notification_body: 'Al parecer no he encontrado ninguna coincidencia con la selección',
        		created_at: new Date().toISOString(),
        		notification_cat: "Error",
        		response_code: 404
        	};
        	res.json(results);
	    }
	});
});

notifications.post("/", function(req, res, next){
	// GET PARAMS WITH BODY_PARSER FOR PERFORM SOME OPERATION
	var notification = req.body;
	var current_date = new Date().toISOString();

	connection.query('INSERT INTO notifications(title, body, id_user, category, created_at) VALUES(?, ?, ?, ?, ?)', [notification.notification_title, notification.notification_body, 1, notification.notification_cat, current_date], function(error, result){
		if(error){
			res.send(error);
		    throw error;
		}else{
			console.log(result);
			res.redirect('/notifications');
		}
 	});
});

notifications.get("/new", function(req, res, next){
	res.sendFile(__dirname + '/views/notifications/new.html');
});

client.get("/", function(req, res, next){
	res.render('client/index.ejs');
});

app.get("/", function(req, res, next){
	console.log("root");
	res.send("root");
});

// ========================================
// ADD OTHERS APPLICATION TO EXPRESS
// ==============================================
// Enables CORS
var enableCORS = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
 
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
 
 
// enable CORS!
//app.use(enableCORS);
app.use('/notifications', notifications);
app.use('/client', client);


// ====================================================== //
// == APP STARTUP
// ====================================================== //
if (process.env.OPENSHIFT_NODEJS_IP && process.env.OPENSHIFT_NODEJS_PORT) {
	http.listen(process.env.OPENSHIFT_NODEJS_PORT, process.env.OPENSHIFT_NODEJS_IP, function() {
  		console.log('Listening at openshift on port: ' + process.env.OPENSHIFT_NODEJS_PORT);
	});
}
else {
	http.listen(3000, function () {
  		console.log('Listing on port: 80')
	});
}